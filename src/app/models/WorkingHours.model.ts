export class WorkingHours {
  public id: string;

  constructor(public year: number,
              public month: number,
              public day: number,
              public hours: number,
              public minutes: number,
              public project: string) {
    this.generateId();
  }

  private generateId() {
    this.id = `${this.day}_${this.month}_${this.year}`;
  }
}
