import {WorkingHours} from '../models/WorkingHours.model';
import {Subject} from 'rxjs';
import {Days} from '../models/Days.enum';
import {Months} from '../models/Months.enum';
import {Projects} from '../models/Projects.enum';
import * as moment from 'moment';
import {HttpClient} from '@angular/common/http';
import {ElementRef, Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class CalendarService {

  workinghrs: WorkingHours[] = [];
  workinghrSubject = new Subject<WorkingHours[]>();

  private days = this.enumToArray(Days);
  private months = this.enumToArray(Months);
  private projects = this.enumToArray(Projects);

  constructor(private httpClient: HttpClient) {
  }

  emitWorkingHours() {
    this.workinghrSubject.next(this.workinghrs);
  }

  addWorkingHours(workinghour) {
    // this.workinghrs.push(workinghour);
    const index = this.workinghrs.findIndex((e) => e.id === workinghour.id);
    if (index === -1) {
      this.workinghrs.push(workinghour);
    } else {
      this.workinghrs[index] = workinghour;
    }
    this.saveWorkingHoursToServer();
    this.emitWorkingHours();
  }

  getDayRangeWeek(i: number, d: number) {
    return [((7 * i) - 7 - d) + 1, ((7 * i) - 7 - d) + 2, ((7 * i) - 7 - d) + 3,
      ((7 * i) - 7 - d) + 4, ((7 * i) - 7 - d) + 5, ((7 * i) - 7 - d) + 6,
      ((7 * i) - 7 - d) + 7];
  }

  getMonthDays(m: number, y: number) {
    return (m === 1) ? 31 : (m === 2) ? (this.leapYear(y) ? 29 : 28) :
      (m === 3) ? 31 : (m === 4) ? 30 : (m === 5) ? 31 : (m === 6) ? 30 :
        (m === 7) ? 31 : (m === 8) ? 31 : (m === 9) ? 30 : (m === 10) ? 31 :
          (m === 11) ? 30 : (m === 12) ? 31 : 0;
  }

  firstDayOfMonth(m, y) {
    const day = moment(`${y}-${m}-${1}`, 'YYYY-MM-DD').format('dddd');
    return this.days.indexOf(day.toString());
  }

  private leapYear(year: number) {
    return year % 100 === 0 ? year % 400 === 0 : year % 4 === 0;
  }

  generateIdFromDate(d: number, m: number, y: number) {
    const g = d * m * y;
    const index = `ID_${g}`;
    return index;
  }

  private enumToArray(e: any) {
    return Object.keys(e)
      .reduce((arr, key) => {
        if (!arr.includes(key)) {
          arr.push(e[key]);
        }
        return arr;
      }, []);
  }

  getDays() {
    return this.days;
  }

  getMonths() {
    return this.months;
  }

  getProjects() {
    return this.projects;
  }

  getWorkingHoursByDay(date: { year: number, month: number, day: number }, begin, pause, end) {
    const workhrs = {hours: 0, minutes: 0};

    const d = date.day;
    const m = date.month;
    const y = date.year;

    const beginHours = begin.toString().split(':')[0];
    const beginMinutes = begin.toString().split(':')[1];

    const endHours = end.toString().split(':')[0];
    const endMinutes = end.toString().split(':')[1];

    const pauseHours = pause.toString().split(':')[0];
    const pauseMinutes = pause.toString().split(':')[1];

    const endTime: Date = new Date(y, m, d, endHours, endMinutes, 0, 0);
    const beginTime: Date = new Date(y, m, d, beginHours, beginMinutes, 0, 0);

    const minutesOfPause = ((pauseHours * 60) + (pauseMinutes * 1));
    const time = endTime.getTime() - beginTime.getTime();
    const WorkingMinutes = ((time / 1000) / 60) - minutesOfPause;

    workhrs.hours = +`${(WorkingMinutes - WorkingMinutes % 60) / 60}`;
    workhrs.minutes = +`${WorkingMinutes % 60}`;

    return workhrs;
  }

  saveWorkingHoursToServer() {
    const url = `${environment.firebaseUrl}/workHrs.json`;
    this.httpClient.put(url, this.workinghrs).subscribe(
      () => {
        console.log('working hours have been saved successfully');
      },
      (err) => {
        console.log(`saving of working hours failed, error: ${err}`);
      });
  }

  getWorkingHoursFromServer() {
    const url = `${environment.firebaseUrl}/workHrs.json`;
    this.httpClient
      .get<any[]>(url)
      .subscribe(
        (response) => {
          if (response !== null) {
            this.workinghrs = response;
            this.emitWorkingHours();
          }
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getWorkHoursById(id: string) {
    // console.log(this.workinghrs);
    const ret = this.workinghrs.find((e) => {
      return e.id === id;
    });
    return ret;
  }

  isExistsWorkHoursById(id: string) {
    const ret = this.workinghrs.find((e) => {
      return e.id === id;
    });
    return ret ? true : false;
  }

  public exportAsExcelFile(excelFileName: string, month: number, year: number): void {
    const json = this.workinghrs.filter((item) => {
      return (item.month === month) && (item.year === year);
    });
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = {Sheets: {data: worksheet}, SheetNames: ['data']};
    const excelBuffer: any = XLSX.write(workbook, {bookType: 'xlsx', type: 'array'});
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
  }

  exportAsPDF(monthCardBody: ElementRef, fileName: string) {
    const data = monthCardBody.nativeElement;
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('monthCardBody/png')
      // A4 size page of PDF
      const pdf = new jsPDF('p', 'mm', 'a4');

      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      // Generated PDF
      pdf.save(fileName);
    });
  }

  printDoc(monthCardBody: ElementRef) {
    let popupWin;
    const data = monthCardBody.nativeElement;
    html2canvas(data).then((canvas) => {
      canvas.toBlob((blob) => {
          const objectURL = URL.createObjectURL(blob);
          popupWin = window.open(objectURL, '_blank', 'top=0,left=0,height=100%,width=auto');
          popupWin.document.open();
          popupWin.document.write(
            `<html>
                  <head>
                  <title>Print Calendar</title>
                  </head>
                  <body onload="window.print();window.close()"><img src="${objectURL}"></body>
                  </html>`
          );
          popupWin.document.close();

        }
        , 'image/png');
    });
  }
}
