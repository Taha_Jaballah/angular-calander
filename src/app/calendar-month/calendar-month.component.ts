import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {CalendarService} from '../services/calendar.service';
import {WorkingHours} from '../models/WorkingHours.model';

@Component({
  selector: 'app-calendar-month',
  templateUrl: './calendar-month.component.html',
  styleUrls: ['./calendar-month.component.scss']
})
export class CalendarMonthComponent implements OnInit, OnDestroy {

  @ViewChild('openModalButton', {static: false}) openModalButton: ElementRef;
  @ViewChild('workHoursModal', {static: false}) workHoursModal: ElementRef;
  @ViewChild('workHoursModalCloseBtn', {static: false}) workHoursModalCloseBtn: ElementRef;
  @ViewChild('monthCardBody', {static: false}) monthCardBody: ElementRef;

  currentDate = {year: 1970, month: 0, day: 0};
  clickedDate = {year: 1990, month: 1, day: 17};
  workHrsForm: FormGroup;

  days = this.calendarService.getDays();
  months = this.calendarService.getMonths();
  projects = this.calendarService.getProjects();


  constructor(private formBuilder: FormBuilder, private calendarService: CalendarService) {
  }

  ngOnInit() {
    this.onFetchWorkingHours();
    this.calendarService.emitWorkingHours();
    this.initCurrentDate();
    this.initForm();
  }

  initForm() {
    this.workHrsForm = this.formBuilder.group({
      begin: ['', Validators.required],
      pause: ['', Validators.required],
      end: ['', Validators.required],
      comment: [''],
      project: ['', Validators.required],
    });
  }

  onWorkHoursSubmit() {
    const workHrs = this.workHrsForm;
    const workinghrsClickedDay = this.calendarService.getWorkingHoursByDay(
      this.clickedDate,
      workHrs.get('begin').value,
      workHrs.get('pause').value,
      workHrs.get('end').value);
    const newWorkHours = new WorkingHours(
      this.clickedDate.year,
      this.clickedDate.month,
      this.clickedDate.day,
      workinghrsClickedDay.hours,
      workinghrsClickedDay.minutes,
      workHrs.value.project
    );
    this.calendarService.addWorkingHours(newWorkHours);
    this.workHoursModalCloseBtn.nativeElement.click();
  }

  onOpenModal() {
    this.openModalButton.nativeElement.click();
  }

  initCurrentDate() {
    const now = moment().format('l').toString().split('/');
    this.currentDate.year = Number(now[2]);
    this.currentDate.month = Number(now[0]);
    this.currentDate.day = Number(now[1]);
  }

  onPreviousMonth(day: number, month: number, year: number) {
    if (month === 1) {
      year = year - 1;
      month = 12;
    } else {
      month = (month - 1);
    }
    this.currentDate.day = day;
    this.currentDate.month = month;
    this.currentDate.year = year;
  }

  onNextMonth(day: number, month: number, year: number) {
    if (month === 12) {
      year = year + 1;
      month = 1;
    } else {
      month = (month + 1);
    }
    this.currentDate.day = day;
    this.currentDate.month = month;
    this.currentDate.year = year;
  }

  getDayRangeWeek(item: number, firstDayOfMounth: any) {
    return this.calendarService.getDayRangeWeek(item, firstDayOfMounth);
  }

  firstDayOfMonth(month: number, year: number) {
    return this.calendarService.firstDayOfMonth(month, year);
  }

  getMonthDays(month: number, year: number) {
    return this.calendarService.getMonthDays(month, year);
  }

  onDayClick(day: number, month: number, year: number, validDay: boolean) {
    if (validDay === true) {
      this.clickedDate.day = day;
      this.clickedDate.month = month;
      this.clickedDate.year = year;
      this.workHrsForm.setValue({
        begin: '08:00',
        end: '17:30',
        pause: '00:30',
        comment: 'New comment',
        project: this.projects[0]
      });
      this.onOpenModal();
    }
  }

  onFetchWorkingHours() {
    this.calendarService.getWorkingHoursFromServer();
  }

  ngOnDestroy(): void {

  }

  setPTextContent(d: number, m: number, y: number) {
    let ret = '';
    const key = `${d}_${m}_${y}`;
    const item = this.calendarService.getWorkHoursById(key);
    if (item) {
      ret = key === item.id ? ` work hours = ${item.hours} : ${item.minutes}` : '';
    }
    return ret;
  }

  onExportPDF() {
    const fileName = `workHours_${this.months[this.currentDate.month - 1]} ${this.currentDate.year}.pdf`;
    this.calendarService.exportAsPDF(this.monthCardBody, fileName);
  }

  onPrint() {
    this.calendarService.printDoc(this.monthCardBody);
  }

  onExportExcel() {
    const fileName = `workHours_${this.months[this.currentDate.month - 1]} ${this.currentDate.year}`;
    this.calendarService.exportAsExcelFile(fileName, this.currentDate.month, this.currentDate.year);
  }

}
